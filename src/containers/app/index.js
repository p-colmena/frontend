import "phoenix_html";
import React from 'react';
import { Link, Route } from 'react-router-dom'
import About from "../about";
import NewJob from "../new-job";
import AppBar from "@material-ui/core/AppBar/AppBar";
import Button from "@material-ui/core/Button/Button";
import Toolbar from '@material-ui/core/Toolbar';
import Typography from "@material-ui/core/Typography/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import PdbViewerPage from "../pdb-viewer-page";
import Jobs from "../jobs"

const styles = {
  main: {
    backgroundColor: '#D8D8D8',
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flex: "1 1",
  },
  navBar: {
    flex: '0 1 auto',
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  root: {
    height: '100%',
    minHeight: '100%',
    display: 'flex',
    flexDirection: 'column'
  }
};

const App = (props) => {
  const { classes } = props;
  return (
    <div className={ classes.root }>
      <AppBar position="static" className={ classes.navBar }>
        <Toolbar>
          <Typography variant="title" color="inherit" className={ classes.grow }>
            Colmena
          </Typography>

          <Button color="inherit" component={ Link } to="/">Jobs</Button>
          <Button color="inherit" component={ Link } to="/new-job">New Job</Button>
          <Button color="inherit" component={ Link } to={ "/about-us" }>About</Button>
        </Toolbar>
      </AppBar>

      < main className={ classes.main }>
        < Route
          exact
          path="/"
          component={ Jobs }
        />
        <Route exact path="/new-job" component={ NewJob }/>
        < Route
          exact
          path="/about-us"
          component={ About }
        />
        <Route
          exact
          path="/viewPdb"
          component={ PdbViewerPage }
        />
      </main>
    </div>
  );
};

export default withStyles(styles)(App);