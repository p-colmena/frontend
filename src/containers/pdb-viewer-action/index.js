import * as React from "react";
import { connect } from "react-redux";
import "./index.css"
import { TaskTypes } from "../task";

class PdbViewerAction extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stage: {},
      components: []
    }
  }

  componentDidMount() {
    this.setState({
      stage: new window.NGL.Stage("pdbPageViewer")
    });
  }

  onActioned() {
    const { actioned } = this.props;
    if (!actioned) {
      return <div/>
    }
  }

  onLoading(loading) {
    if (loading) {
      return <p>"Loading..."</p>
    }
  }

  onError(error) {
    if (error) {
      return <p>Error, please try it later.</p>
    }
  }

  loadPdb(success) {
    const configuration = new TaskTypes(success.task).case({
      structure: () => (component) => {
        component.addRepresentation("hyperball");
      },
      surface: () => (component) => {
        component.addRepresentation("hyperball");
      },
      pockets: () => (component) => {
        component.addRepresentation("hyperball");
      },
      pocket: () => (component) => {
        component.addRepresentation("surface");
      },
    });

    const maybeComponent = this.state.components
      .find(component => component.task.taskType === success.task.taskType && component.task.id === success.task.id);

    if (!maybeComponent) {
      this.state.stage.loadFile(new Blob([ success.pdb ], { type: 'text/plain' }), {
        ext: 'pdb'
      })
        .then(component => {
          this.state.components.push({ task: success.task, component: component });
          configuration(component);
          component.autoView()
        });
    }
  }

  onSuccess(success) {
    if (success) {
      this.loadPdb(success);
    }
    return (
      <div/>
    );
  }

  toggle(_toggle) {
    if (_toggle) {
      const component = this.state.components
        .find(component => component.task.taskType === _toggle.task.taskType && component.task.id === _toggle.task.id);
      if (component !== undefined) {
        component.component.setVisibility(_toggle.toggle)
      }
    }
  }

  render() {
    const { actioned, loading, success, error, toggle } = this.props;
    return (
      <div style={ { height: '100%' } }>
        { this.onActioned(actioned) }
        { this.onLoading(loading) }
        { this.onSuccess(success) }
        { this.onError(error) }
        { this.toggle(toggle) }
        <div style={ { height: 'calc(100vh - 4rem)' } } id="pdbPageViewer">
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ( {
  actioned: state.displayPdbData.actioned,
  toggle: state.displayPdbData.toggle,
  success: state.displayPdbData.success,
  loading: state.displayPdbData.loading,
  error: state.displayPdbData.error
} );

export default connect(mapStateToProps)(PdbViewerAction)