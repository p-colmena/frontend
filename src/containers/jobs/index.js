import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import { fetchJobs } from "../../modules/jobs";
import Button from "@material-ui/core/Button/Button";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import CheckIcon from '@material-ui/icons/Check';
import CancelIcon from '@material-ui/icons/CancelOutlined';
import ArrowFowardIcon from '@material-ui/icons/ArrowForward';
import Card from "@material-ui/core/Card/Card";
import CardContent from "@material-ui/core/CardContent/CardContent";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid/Grid";
import Typography from "@material-ui/core/Typography/Typography";
import Divider from "@material-ui/core/Divider/Divider";
import SearchIcon from '@material-ui/icons/Search';
import TextField from "@material-ui/core/TextField/TextField";

const styles = {
  grid: {
    padding: '20px'
  },
  gridTitle: {
    textAlign: 'center',
  },
  gridHeader: {
    textAlign: 'center',
    padding: '15px',
  },
  gridRow: {
    padding: '5px',
    textAlign: 'center',
  },
  error: {
    backgroundColor: "rgba(255, 0, 0, 0.1)",
  },
  success: {
    backgroundColor: "rgba(0, 128, 0, 0.1)"
  },
  running: {
    backgroundColor: "rgba(0, 0, 128, 0.1)"
  },
};

class Jobs extends React.Component {

  constructor(props) {
    super(props);
    this.timeout = 0;
    this.srcollTimeout = 0;
    this.state = {
      selectedId: null,
      openErrorDetails: false,
      selected: [],
      page: 0,
      searchTerm: ""
    };
  }

  componentDidMount() {
    this.props.dispatch(fetchJobs());
  }

  errorDetails(job) {
    this.setState({
      openErrorDetails: true,
      selectedJob: job
    });
  }

  detailsButton(job) {
    switch (job.state) {
      case "success":
        return (
          <Button size="medium" color="primary" type="submit" component={ Link }
                  to={ `/viewPdb?pdbId=${ job.pdbId }` }>
            details
          </Button> );
      case "error":
        return (
          <Button size="medium" color="primary" type="submit" onClick={ () => this.errorDetails(job) }>details</Button>
        );
      default:
        return (
          <Button size="medium" color="primary" disabled={ true } type="submit">details</Button>
        );
    }
  }

  detailsError() {
    if (this.state.openErrorDetails && this.state.selectedJob) {
      return (
        <Dialog
          open={ this.state.openErrorDetails }
          onClose={ () => this.setState({ openErrorDetails: false }) }
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{ "Error detail" }</DialogTitle>
          <DialogContent>
            <DialogContentText>
              { this.state.selectedJob.result }
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={ () => this.setState({ openErrorDetails: false }) } color="primary" autoFocus>
              close
            </Button>
          </DialogActions>
        </Dialog>
      );
    }
  }

  jobState(job) {
    switch (job.state) {
      case "success":
        return (
          <CheckIcon/>
        );
      case "error":
        return (
          <CancelIcon/>
        );
      case "running":
        return (
          <ArrowFowardIcon/>
        );
      default:
        return (
          <div/>
        );
    }
  }

  render() {
    const { loading, classes } = this.props;
    return (
      <Card>
        { this.detailsError() }
        <CardContent>
          <Grid className={ classes.grid } container direction="column" justify="center">
            <Grid className={ classes.gridTitle } item xs={ 12 }>
              <Typography variant='display2' gutterBottom>Jobs</Typography>
            </Grid>
            <Grid item xs={ 12 }>
              <Grid container direction='row' alignItems='flex-end'>
                <Grid item>
                  <SearchIcon/>
                </Grid>
                <Grid item>
                  <TextField
                    placeholder="Search…"
                    onKeyUp={ this.searchJobs() }
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={ 12 }>
              <Grid container direction="row" justify="space-evenly">
                <Grid item xs={ 12 }>
                  <Grid className={ classes.gridHeader } container direction="row">
                    <Grid item xs={ 2 }>
                      <Typography variant='title'>PDB Id</Typography>
                    </Grid>
                    <Grid item xs={ 3 }>
                      <Typography variant='title'>Name</Typography>
                    </Grid>
                    <Grid item xs={ 2 }>
                      <Typography variant='title'>State</Typography>
                    </Grid>
                    <Grid item xs={ 3 }>
                      <Typography variant='title'>Duration</Typography>
                    </Grid>
                    <Grid item xs={ 2 }>
                      <Typography variant='title'>Action</Typography>
                    </Grid>
                  </Grid>
                  <Grid container direction="row">
                    <Grid item xs={ 12 }>
                      <Divider variant='inline'/>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={ 12 }>
                  { this.jobsList() }
                  { this.onLoading(loading) }
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    );
  }

  jobsList() {
    const { error, jobs, classes } = this.props;

    if (error) {
      return (
        <Grid container direction="column">
          <Grid item xs={ 12 }>
            <Typography variant='headline'>Error! { error.message }</Typography>
          </Grid>
        </Grid>
      )
    }

    debugger;
    return jobs.map((job, index) => {
      return (
        <Grid container direction="column" key={ index }>
          <Grid className={ `${ classes.gridRow } ${ classes[ job.state ] }` }
                container
                direction="row"
                alignItems='center'>
            <Grid item xs={ 2 }>
              <Typography variant='subheading'>{ job.pdbId }</Typography>
            </Grid>
            <Grid item xs={ 3 }>
              <Typography variant='body1'>{ job.name }</Typography>
            </Grid>
            <Grid item xs={ 2 }>{ this.jobState(job) }</Grid>
            <Grid item xs={ 3 }>
              <Typography variant='body1'>{ job.duration }</Typography>
            </Grid>
            <Grid item xs={ 2 }>{ this.detailsButton(job) }</Grid>
          </Grid>
          <Grid container direction="row">
            <Grid item xs={ 12 }>
              <Divider variant='inline'/>
            </Grid>
          </Grid>
        </Grid>
      );
    })
  }

  onLoading(loading) {
    if (loading) {
      return (
        <Grid container direction="column">
          <Grid item xs={ 12 }>
            <Typography variant='headline'>Loading...</Typography>
          </Grid>
        </Grid>
      )
    } else {
      return <div/>
    }
  }

  searchJobs() {
    return (e) => {
      const value = e.target.value;
      if (this.timeout) clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        this.props.dispatch(fetchJobs(value));
      }, 1000)
    }
  }
}

const mapStateToProps = state => ( {
  jobs: state.jobs.items,
  loading: state.jobs.loading,
  error: state.jobs.error
} );

export default connect(mapStateToProps)(withStyles(styles)(Jobs));
