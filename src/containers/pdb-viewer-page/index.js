import * as React from "react";
import connect from "react-redux/es/connect/connect";
import qs from "query-string"
import "./index.css"
import { fetchTasks } from "../../modules/tasks";
import { Task } from "../task";
import PdbViewerAction from "../pdb-viewer-action";
import Grid from "@material-ui/core/Grid/Grid";

class PdbViewerPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pdbId: ""
    }
  }

  componentDidMount() {
    const { pdbId } = qs.parse(this.props.location.search);
    this.props.dispatch(fetchTasks(pdbId));
    this.setState({
      pdbId: pdbId
    });
  }

  showTasks(tasks) {
    if (tasks) {
      return (
        <Grid container style={ { width: 'inherit' } } direction="column">
          { tasks.map((task, index) => {
            return (
              <Task index={ index } task={ task }/>
            )
          }) }
        </Grid>
      )
    } else {
      return <div/>
    }
  }

  showLoading(loading) {
    if (loading) {
      return <p>Loading...</p>
    }
  }

  showError(error) {
    if (error) {
      return <p>Error please try again later</p>
    }
  }

  render() {
    const { tasks, loading, error } = this.props;
    return (
      <Grid container>
        <Grid style={ {
          maxHeight: 'calc(100vh - 4rem)',
          overflowY: 'auto'
        } } item xs={ 3 }>
          { this.showTasks(tasks) }
          { this.showLoading(loading) }
          { this.showError(error) }
        </Grid>
        <Grid style={ {
          maxHeight: 'calc(100vh - 4rem)',
          overflowY: 'hidden'
        } } item xs={ 9 }>
          <PdbViewerAction/>
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = state => ( {
  tasks: state.tasks.tasks,
  loading: state.tasks.loading,
  error: state.tasks.error,
} );

export default connect(mapStateToProps)(PdbViewerPage);