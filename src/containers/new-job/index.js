import React from 'react';
import connect from "react-redux/es/connect/connect";
import { withStyles } from "@material-ui/core";
import TextField from "@material-ui/core/TextField/TextField";
import Card from "@material-ui/core/Card/Card";
import CardContent from "@material-ui/core/CardContent/CardContent";
import Typography from "@material-ui/core/Typography/Typography";
import CardActions from "@material-ui/core/CardActions/CardActions";
import Button from "@material-ui/core/Button/Button";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import { submitJob } from "../../modules/new-job";
import Fade from "@material-ui/core/Fade/Fade";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import SnakbarSuccess from "../snackbar";

const classes = {
  card: {
    minWidth: 275,
    maxWidth: 640
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  field: {
    marginTop: 15,
    marginBottom: 15
  },
};

class NewJob extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.initialState()
  }

  initialState() {
    return {
      jobName: "",
      pdbId: "",
      structure: false,
      surface: false,
      pockets: false,
      formValid: false,
      formErrors: {
        jobName: false,
        pdbId: false,
      }
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    console.log(this.state);
    this.props.dispatch(submitJob({
      name: this.state.jobName,
      pdbId: this.state.pdbId,
      structure: this.state.structure,
      surface: this.state.surface,
      pockets: this.state.pockets
    }));
  }

  handleInputChange(field) {
    return (event) => {
      this.setState({
        [ field ]: event.target.value
      });
    }
  };

  validateInput(field) {
    const stateField = this.state[ field ];
    this.setState((state) => {
      const formErrors = { ...state.formErrors, ...{ [ field ]: stateField.length <= 0 } };
      return {
        formErrors: formErrors,
      }
    });
    this.validateForm();
  }

  validateForm() {
    this.setState((state) => {
      return {
        formValid: ( state.pdbId.length > 0 ) && ( state.jobName.length > 0 )
      }
    })
  }

  tasks() {
    const structure =
      <Typography component="p">
        Get PDB structure?
        <Checkbox
          onChange={ () => this.setState((state) => {
            return { structure: !state.structure }
          }) }
        />
      </Typography>;

    const surface =
      <Typography component="p">
        Measure PDB surface?
        <Checkbox
          onChange={ () => this.setState((state) => {
            return { surface: !state.surface }
          }) }
        />
      </Typography>;

    const pockets =
      <Typography component="p">
        Search for pockets?
        <Checkbox
          onChange={ () => this.setState((state) => {
            return { pockets: !state.pockets }
          }) }
        />
      </Typography>;

    if (this.state.pdbId.length > 0) {
      if (this.state.structure) {
        return (
          <div>
            { structure }
            <br/>
            { surface }
            <br/>
            { pockets }
          </div>
        )
      }
      else {
        return ( <div>{ structure } </div> )
      }
    }
  }

  loading(isLoading) {
    const loading =
      <div className={ isLoading ? "loading-image" : "" }>
        <Fade in={ isLoading } unmountOnExit>
          <CircularProgress/>
        </Fade>
      </div>;

    return ( isLoading ? loading : <div/> );
  }

  succcess(job) {
    const success =
      <SnakbarSuccess open={ true } message={ "Success!!!" } variant="success"/>;
    return ( job ? success : <div/> )
  }

  error(error) {
    const errorSnackbar =
      <SnakbarSuccess open={ true } message={ "Error!!!" } variant="error"/>;
    return ( error ? errorSnackbar : <div/> )
  }

  render() {
    const { classes, job, loading, error } = this.props;

    return (
      <div>
        { this.loading(loading) }
        { this.succcess(job) }
        { this.error(error) }
        <form className={ loading ? "loading" : "" } onSubmit={ this.handleSubmit.bind(this) }>
          <Card className={ classes.card }>
            <CardContent>
              <Typography className={ classes.title } color="textSecondary">
                Submit new job
              </Typography>

              <Typography variant="headline" component="h2">
                Complete a pdb id and a name to the job
              </Typography>

              <TextField
                label="Pdb Id"
                error={ this.state.formErrors.pdbId }
                helperText={ this.state.formErrors.pdbId ? "Specify a pdb id" : "" }
                className={ classes.field }
                value={ this.state.pdbId }
                onChange={ this.handleInputChange("pdbId") }
                onBlur={ () => this.validateInput("pdbId") }
              />

              <br/>
              <TextField
                error={ this.state.formErrors.jobName }
                helperText={ this.state.formErrors.jobName ? "Specify a job name" : "" }
                label="Job name"
                className={ classes.field }
                value={ this.state.jobName }
                onChange={ this.handleInputChange("jobName") }
                onBlur={ () => this.validateInput("jobName") }
              />
              <br/>
              { this.tasks() }
            </CardContent>

            <CardActions>
              <Button size="medium" color="primary" type="submit" disabled={ !this.state.formValid }>Submit</Button>
            </CardActions>
          </Card>
        </form>
      </div>
    );
  }

  formValid() {
    return this.state.formErrors.jobName && this.state.formErrors.pdbId
  }
}

const mapStateToProps = state => {
  return ( {
    job: state.newJob.job,
    loading: state.newJob.loading,
    error: state.newJob.error
  } );
};

export default connect(mapStateToProps)(withStyles(classes)(NewJob));