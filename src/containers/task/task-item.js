import * as React from "react";
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import EyeIcon from '@material-ui/icons/Visibility';
import ClosedEyeIcon from '@material-ui/icons/VisibilityOff';
import IconButton from "@material-ui/core/IconButton/IconButton";
import "./index.css"

export class TaskDownloadIcon extends React.Component {
  render() {
    return (
      <IconButton onClick={ this.props.action }>
        <CloudDownloadIcon/>
      </IconButton>
    )
  }
}

export class TaskDisplayIcon extends React.Component {
  render() {
    return (
      <IconButton onClick={ this.props.action }>
        <EyeIcon/>
      </IconButton>
    )
  }
}

export class TaskClosedIcon extends React.Component {
  render() {
    return (
      <IconButton onClick={ this.props.action }>
        <ClosedEyeIcon/>
      </IconButton>
    )
  }
}