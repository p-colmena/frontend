import AbstractTask from "./abstract-task";
import * as React from "react";
import { TaskDownloadIcon } from "./task-item";
import connect from "react-redux/es/connect/connect";
import { service } from "../../modules/service";
import Grid from "@material-ui/core/Grid/Grid";
import ExpansionPanel from "@material-ui/core/ExpansionPanel/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails/ExpansionPanelDetails";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

class TaskPocket extends AbstractTask {

  render() {
    return (
      <Grid item xs={ 12 }>
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={ <ExpandMoreIcon/> }>
            <Typography className="tasks__heading">{ `Pocket ${ this.props.index.toString() }` }</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <TaskDownloadIcon action={ () => this.downloadPocket(this.props.task) }/>
            { this.displayAction(this.props.task) }
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </Grid>
    )
  }

  downloadPocket(pocket) {
    service.downloadPocket(pocket.id)
  }
}

export default connect()(TaskPocket)
