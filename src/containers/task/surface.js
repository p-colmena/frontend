import * as React from "react";
import { TaskDownloadIcon } from "./task-item";
import { service } from "../../modules/service";
import connect from "react-redux/es/connect/connect";
import "./index.css"
import AbstractTask from "./abstract-task";
import Grid from "@material-ui/core/Grid/Grid";
import ExpansionPanel from "@material-ui/core/ExpansionPanel/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails/ExpansionPanelDetails";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

class TaskSurface extends AbstractTask {

  render() {
    return (
      <Grid container direction="row">
        <Grid item xs={ 12 }>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={ <ExpandMoreIcon/> }>
              <Typography className="tasks__heading">PDB Surface</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <TaskDownloadIcon action={ () => this.downloadSurface() }/>
                { this.displayAction(this.props.task) }
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
      </Grid>
    )
  }

  downloadSurface() {
    return service.downloadSurface(this.props.task.pdbSurfaceId);
  }
}

export default connect()(TaskSurface)