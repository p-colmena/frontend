import * as React from "react";
import { TaskClosedIcon, TaskDisplayIcon } from "./task-item";
import { displayPdbData, togglePdbData } from "../../modules/display-structure";

class AbstractTask extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      firstTime: true,
      hidden: false,
    }
  }

  displayAction(task) {
    const hidden = this.state.hidden;

    if (this.state.firstTime) {
      return ( <TaskDisplayIcon action={ () => {
        this.setState({ hidden: false, firstTime: false });
        this.props.dispatch(displayPdbData(task))
      } }/> )
    } else {
      if (hidden) {
        return (
          <TaskClosedIcon action={ () => {
            this.setState({ hidden: false });
            this.props.dispatch(togglePdbData(hidden, task))
          } }/>
        )
      } else {
        return (
          <TaskDisplayIcon action={ () => {
            this.setState({ hidden: true });
            this.props.dispatch(togglePdbData(hidden, task))
          } }/>
        )
      }
    }
  }

}

export default AbstractTask