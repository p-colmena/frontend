import * as React from "react";
import { TaskDownloadIcon } from "./task-item";
import { service } from "../../modules/service";
import connect from "react-redux/es/connect/connect";
import "./index.css"
import AbstractTask from "./abstract-task";
import TaskPocket from "./pocket";
import Grid from "@material-ui/core/Grid/Grid";
import ExpansionPanel from "@material-ui/core/ExpansionPanel/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails/ExpansionPanelDetails";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

class TaskPockets extends AbstractTask {

  render() {

    const pocketResult =
      <Grid container direction="row">
        <Grid item xs={ 12 }>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={ <ExpandMoreIcon/> }>
              <Typography className="tasks__heading">Pockets hunt result</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Typography>
                <TaskDownloadIcon action={ () => this.downloadPocketHuntResult() }/>
                { this.displayAction(this.props.task) }
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
        <Grid item xs={ 12 }>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={ <ExpandMoreIcon/> }>
              <Typography className="tasks__heading">Pocket list</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container direction="column">
                { this.props.task.pockets
                  .map((pocket, index) => {
                    return <TaskPocket task={ pocket } index={ index }/>;
                  })
                }
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
      </Grid>;

    return ( [ pocketResult ] )
  }

  downloadPocketHuntResult() {
    service.downloadPocketHunt(this.props.task.pdbPocketsId)
  }
}

export default connect()(TaskPockets)