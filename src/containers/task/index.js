import * as React from "react";
import TaskStructure from "./structure";
import TaskSurface from "./surface";
import TaskPockets from "./pockets";

export class Task extends React.Component {
  render() {
    const { task, index } = this.props;
    return (
      TaskTypes.case(task, {
        structure: () => <TaskStructure index={ index } task={ task }/>,
        surface: () => <TaskSurface index={ index } task={ task }/>,
        pockets: () => <TaskPockets index={ index } task={ task }/>
      })
    )
  }
}

export class TaskTypes {
  static case(task, cases) {
    return new TaskTypes(task).case(cases)
  }

  constructor(task) {
    this.type = task.taskType
  }

  case({ structure, surface, pockets, pocket }) {
    switch (this.type) {
      case "structure":
        return this.matchTaskType(structure)();
      case "surface":
        return this.matchTaskType(surface)();
      case "pockets":
        return this.matchTaskType(pockets)();
      case "pocket":
        return this.matchTaskType(pocket)();
      default:
        throw Error(`Task type not contempled ${this.type}`)
    }
  }

  matchTaskType(typeAction) {
    if (typeof typeAction !== "function") {
      return () => {
        throw Error(`Task type not contempled ${this.type}`)
      }
    } else {
      return typeAction
    }
  }
}
