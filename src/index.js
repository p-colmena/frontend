import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import Provider from "react-redux/es/components/Provider";
import App from "./containers/app";
import { ConnectedRouter } from 'connected-react-router'
import store, { history } from './store'

const target = document.getElementById('root');

const Application =
  <Provider store={ store }>
    <ConnectedRouter history={ history }>
      <App/>
    </ConnectedRouter>
  </Provider>;

ReactDOM.render(Application, target);
registerServiceWorker();
