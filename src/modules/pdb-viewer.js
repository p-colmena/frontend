import { service } from "./service";

const FETCH_PDB_BEGIN = 'pdb/FETCH_PDB_BEGIN';
const FETCH_PDB_SUCCESS = 'pdb/FETCH_PDB_SUCCESS';
const FETCH_PDB_FAILURE = 'pdb/FETCH_PDB_FAILURE';

export const fetchPdbBegin = () => ( {
  type: FETCH_PDB_BEGIN
} );

export const fetchPdbSuccess = pdb => ( {
  type: FETCH_PDB_SUCCESS,
  payload: { pdb }
} );

export const fetchPdbFailure = error => ( {
  type: FETCH_PDB_FAILURE,
  payload: { error }
} );

export function fetchPdb(taskType, pdbId) {
  return dispatch => {
    dispatch(fetchPdbBegin());
    return service.fetchPdb(taskType, pdbId)
      .then(pdb => {
        dispatch(fetchPdbSuccess(pdb.data));
        return pdb
      })
      .catch(error => dispatch(fetchPdbFailure(error)));
  };
}

const initialState = {
  pdb: null,
  loading: true,
  error: null
};

export default function pdbViewerReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PDB_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_PDB_SUCCESS:
      return {
        ...state,
        loading: false,
        pdb: action.payload.pdb
      };

    case FETCH_PDB_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        pdb: null
      };

    default:
      return state;
  }
}