import { service } from "./service";
import { TaskTypes } from "../containers/task";

const DISPLAY_PDB_DATA_BEGIN = 'display/DISPLAY_PDB_DATA_BEGIN';
const DISPLAY_PDB_DATA_SUCCESS = 'display/DISPLAY_PDB_DATA_SUCCESS';
const DISPLAY_PDB_DATA_FAILURE = 'display/DISPLAY_PDB_DATA_FAILURE';
const TOGGLE_PDB_DATA = 'display/TOGGLE_PDB_DATA';

export const displayPdbDataBegin = () => ( {
  type: DISPLAY_PDB_DATA_BEGIN
} );

export const displayPdbDataSuccess = (task, pdb) => ( {
  type: DISPLAY_PDB_DATA_SUCCESS,
  payload: { task: task, pdb: pdb }
} );

export const togglePdbData = (toggle, task) => ( {
  type: TOGGLE_PDB_DATA,
  payload: { task: task, toggle: toggle }
} );

export const displayPdbDataFailure = error => ( {
  type: DISPLAY_PDB_DATA_FAILURE,
  payload: { error }
} );

export function displayPdbData(task) {
  const pdbPromise = new TaskTypes(task).case({
    structure: () => service.displayStructure(task.pdbStructureId),
    surface: () => service.displaySurface(task.pdbSurfaceId),
    pockets: () => service.displayPocketsHunt(task.pdbPocketsId),
    pocket: () => service.displayPocket(task.id)
  });

  return dispatch => {
    dispatch(displayPdbDataBegin());
    return pdbPromise
      .then(pdb => {
        dispatch(displayPdbDataSuccess(task, pdb.data));
        return pdb
      })
      .catch(error => dispatch(displayPdbDataFailure(error)));
  };
}

const initialState = {
  actioned: false,
  pdb: null,
  loading: false,
  error: null,
  toggle: null
};

export default function displayPdbDataReducer(state = initialState, action) {
  switch (action.type) {
    case DISPLAY_PDB_DATA_BEGIN:
      return {
        ...state,
        loading: true,
        actioned: true,
        toggle: null,
        error: null
      };

    case DISPLAY_PDB_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        toggle: null,
        success: { task: action.payload.task, pdb: action.payload.pdb }
      };

    case DISPLAY_PDB_DATA_FAILURE:
      return {
        ...state,
        loading: false,
        toggle: null,
        error: action.payload.error,
        pdb: null
      };

    case TOGGLE_PDB_DATA:
      return {
        ...state,
        success: null,
        toggle: {task: action.payload.task, toggle: action.payload.toggle}
      };

    default:
      return state;
  }
}