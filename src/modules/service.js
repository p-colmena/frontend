import axios from "axios";
import * as moment from "moment";

class Service {
  constructor() {
    this.baseUrl = 'http://localhost:4000';
    this.axios = axios.create({
      baseURL: this.baseUrl,
      timeout: 10000
    });
  }

  fetchPdb(taskType, pdbId) {
    return this.axios.get(`/api/pdb/download?taskType=${taskType}&pdbId=${pdbId}`)
  }

  downloadStructure(structureId) {
    window.open(`${this.baseUrl}/api/pdb/structure?pdbStructureId=${structureId}`, '_blank')
  }

  displayStructure(structureId) {
    return this.axios.get(`/api/pdb/structure?pdbStructureId=${structureId}`)
  }

  downloadSurface(surfaceId) {
    window.open(`${this.baseUrl}/api/pdb/surface?pdbSurfaceId=${surfaceId}`, '_blank')
  }

  displaySurface(surfaceId) {
    return this.axios.get(`/api/pdb/surface?pdbSurfaceId=${surfaceId}`)
  }

  downloadPocketHunt(pocketsId) {
    window.open(`${this.baseUrl}/api/pdb/pockets?pdbPocketsId=${pocketsId}`, '_blank')
  }

  displayPocketsHunt(pocketsId) {
    return this.axios.get(`/api/pdb/pockets/display?pdbPocketsId=${pocketsId}`)
  }

  downloadPocket(pocketId) {
    window.open(`${this.baseUrl}/api/pdb/pocket?id=${pocketId}`, '_blank')
  }

  displayPocket(pocketId) {
    return this.axios.get(`/api/pdb/pocket?id=${pocketId}`)
  }

  fetchTasks(pdbId) {
    return this.axios.get(`/api/task/all?pdbId=${pdbId}`)
      .then(json => json.data.tasks)
  }

  newJob(newJob) {
    return this.axios.post('/api/run', {
      name: newJob.name,
      pdbId: newJob.pdbId,
      tasks: this.mapTasks(newJob)
    })
      .then(json => json.data.data)
  }

  createTask(task = "", subtasks = []) {
    return { task: task, subtasks: subtasks }
  }

  mapTasks(newJob) {
    let tasks = {};
    if (newJob.structure) {
      tasks = this.createTask("structure")
    }
    if (newJob.surface) {
      tasks.subtasks.push(this.createTask("surface"))
    }
    if (newJob.pockets) {
      tasks.subtasks.push(this.createTask("pockets"))
    }
    return tasks;
  }

  fetchJobs(searchTerm = "", page = 0) {
    return this.axios.get(`/api/jobs?searchTerm=${searchTerm}&page=${page}`)
      .then(json => json.data)
      .then(data => {
        data.jobs = this.mapJobs(data.jobs);
        return data;
      })
  }

  mapJobs(data) {
    data.map(d => {
      d.duration = moment.duration(d.duration).humanize();
      return d;
    });

    return data;
  };
}

let service = new Service();
export { service }