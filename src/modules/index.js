import { combineReducers } from 'redux'
import jobsReducer from "./jobs";
import newJobReducer from "./new-job"
import tasksReducer from "./tasks";
import pdbViewerReducer from "./pdb-viewer";
import displayPdbDataReducer from "./display-structure";

export default combineReducers(
  {
    jobs: jobsReducer,
    newJob: newJobReducer,
    tasks: tasksReducer,
    pdbViewer: pdbViewerReducer,
    displayPdbData: displayPdbDataReducer
  }
)