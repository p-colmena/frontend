import { service } from "./service";

const NEW_JOB_BEGIN = 'new-job/NEW_JOB_BEGIN';
const NEW_JOB_SUCCESS = 'jobs/NEW_JOB_SUCCESS';
const NEW_JOB_FAILURE = 'jobs/NEW_JOB_FAILURE';

export const submitJobBegin = () => ( {
  type: NEW_JOB_BEGIN
} );

export const submitJobSuccess = (job) => ( {
  type: NEW_JOB_SUCCESS,
  payload: { job }
} );

export const submitJobFailure = error => ( {
  type: NEW_JOB_FAILURE,
  payload: { error }
} );

export function submitJob(newJob) {
  return dispatch => {
    dispatch(submitJobBegin());
    return service.newJob(newJob)
      .then(job => {
        dispatch(submitJobSuccess(job));
        return job
      })
      .catch(error => dispatch(submitJobFailure(error)));
  };
}

const initialState = {
  job: null,
  loading: false,
  error: null
};

export default function newJobReducer(state = initialState, action) {
  switch (action.type) {
    case NEW_JOB_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };

    case NEW_JOB_SUCCESS:
      return {
        ...state,
        loading: false,
        job: action.payload.job
      };

    case NEW_JOB_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        job: {}
      };

    default:
      return state;
  }
}