import { service } from "./service";

const FETCH_TASKS_BEGIN = 'tasks/FETCH_TASKS_BEGIN';
const FETCH_TASKS_SUCCESS = 'tasks/FETCH_TASKS_SUCCESS';
const FETCH_TASKS_FAILURE = 'tasks/FETCH_TASKS_FAILURE';

export const fetchTasksBegin = () => ( {
  type: FETCH_TASKS_BEGIN
} );

export const fetchTasksSuccess = tasks => ( {
  type: FETCH_TASKS_SUCCESS,
  payload: { tasks }
} );

export const fetchTasksFailure = error => ( {
  type: FETCH_TASKS_FAILURE,
  payload: { error }
} );

export function fetchTasks(pdbId) {
  return dispatch => {
    dispatch(fetchTasksBegin());
    return service.fetchTasks(pdbId)
      .then(job => {
        dispatch(fetchTasksSuccess(job));
        return job
      })
      .catch(error => dispatch(fetchTasksFailure(error)));
  };
}

const initialState = {
  tasks: null,
  loading: false,
  error: null
};

export default function tasksReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_TASKS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_TASKS_SUCCESS:
      return {
        ...state,
        loading: false,
        tasks: action.payload.tasks
      };

    case FETCH_TASKS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        tasks: null
      };

    default:
      return state;
  }
}