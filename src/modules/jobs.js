import { service } from "./service";

const FETCH_JOBS_BEGIN = 'jobs/FETCH_JOBS_BEGIN';
const FETCH_JOBS_SUCCESS = 'jobs/FETCH_JOBS_SUCCESS';
const FETCH_JOBS_FAILURE = 'jobs/FETCH_JOBS_FAILURE';

export const fetchJobsBegin = () => ( {
  type: FETCH_JOBS_BEGIN
} );

export const fetchJobsSuccess = jobs => ( {
  type: FETCH_JOBS_SUCCESS,
  payload: { jobs }
} );

export const fetchJobsFailure = error => ( {
  type: FETCH_JOBS_FAILURE,
  payload: { error }
} );

export function fetchJobs(searchTerm) {
  return dispatch => {
    dispatch(fetchJobsBegin());
    return service.fetchJobs(searchTerm)
      .then(jobs => {
        dispatch(fetchJobsSuccess(jobs));
        return jobs
      })
      .catch(error => {
        dispatch(fetchJobsFailure(error))
      });
  };
}

const initialState = {
  items: [],
  loading: true,
  error: null
};

export default function jobsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_JOBS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_JOBS_SUCCESS:
      return {
        ...state,
        loading: false,
        items: action.payload.jobs.jobs
      };

    case FETCH_JOBS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };

    default:
      return state;
  }
}