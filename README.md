# Colmena
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Prerequisites:
   * [NodeJs](https://nodejs.org/es/) 10.9.0 or higher
   * [npm](https://www.npmjs.com/) 6.4.1 or higher

To start your Development server:

  * Install dependencies with `npm install`
  * Be sure to have your backend running in localhost:4000
  * Start Node server with `npm start`

Now you can visit [localhost:3000](http://localhost:3000) from your browser.

For more information please refer to the [backend project](https://gitlab.com/p-colmena/backend) or contact me [garciasmithagustin@gmail.com](garciasmithagustin@gmail.com)

